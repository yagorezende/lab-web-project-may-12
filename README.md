# README Codex Tutorials #
Universidade Federal Fluminese.
Niterói, 12 de Maio de 2017.

Essa aplicação foi desenvolvida para aula de Laboratório de Programação Web turma BA.
Projeto desenvolvido por Yago de Rezende dos Santos.

### Infos ###

* Application: 1.0
* Bootstrap css version: 3.3.7
* Bootstrap js version: 
* JQuery version: 3.2.1
 
### Importante! ###
1. Para a aplicação funcionar deve-se abri-la pelo WebStorm ou usando o Mozilla Firefox(também funciona), para que os scripts funcionem.
2. Por algum motivo os arquivos do bootstrap estavam bugando, então tive que usar os links disponibilizados pelo Bootstrap.